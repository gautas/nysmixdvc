.PHONY: dev_image dev_container book serve_book pull extract load test test_all bump

dev_image:
	docker build -t nysmixdvc -f dev.dockerfile .

dev_container:
	docker run -it --rm -d --name nysmixdvc --env-file .env.container -v $(shell pwd):/nysmixdvc nysmixdvc