FROM python:3.10

# define a user variable
ENV DOCKER_USER devuser

# make the user group
RUN addgroup --gid 1222 devgroup

# create the user
RUN adduser \
  --disabled-password \
  --gecos "" \
  --ingroup devgroup \
  "$DOCKER_USER"

# set the user
USER "$DOCKER_USER"

# install poetry for dependency management https://python-poetry.org/docs/master/#installation
RUN curl -sSL https://install.python-poetry.org | python3 - --version 1.1.13

# add location of poetry binary to the path
ENV PATH="/root/.local/bin:${PATH}"

# venvs in folder
ENV POETRY_VIRTUALENVS_IN_PROJECT=true